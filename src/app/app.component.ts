import { Component } from '@angular/core'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  message = `WEBSITE COMING SOON`
  legalSpeel = `Origin Environmental Arboriculture is a trading name of Origin Environmental Arboriculture Ltd which is registered in England & Wales under Company Registration No. 13618647`
  mailHref = `mailto:hello@origin-arb.com?subject=👋 Hi there`
  igHref = `https://www.instagram.com/origin.environmental/`
}
